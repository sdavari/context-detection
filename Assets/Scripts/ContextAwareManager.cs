﻿using Microsoft.MixedReality.Toolkit.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContextAwareManager : MonoBehaviour
{
	public GameObject VirtualApp;
	public GameObject FaceDetectionObject;
	private FaceDetection faceDetection;
	private bool isFace;
	public bool isTalking;
	private OpenCvSharp.Rect facePos;
	private float faceScale;
	private float confidence;
	private int appIsTrans;

	private void Awake()
	{
		faceDetection = FaceDetectionObject.GetComponent<FaceDetection>();
		isTalking = false;
		isFace = false;
		facePos = new OpenCvSharp.Rect();
		faceScale = 0;
		appIsTrans = 0;
	}

	private void Update()
	{
		if (!isTalking)
		{
			VirtualApp.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
		}
		else
		{
			VirtualApp.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 100);
			if (FaceDetection.DetectFaces(out facePos, out faceScale, out confidence))
			{
				//Vector3 appScreenPos = Camera.main.WorldToScreenPoint(VirtualApp.gameObject.transform.position);

				float minX1 = VirtualApp.GetComponent<SpriteRenderer>().sprite.bounds.min.x;
				float minY1 = VirtualApp.GetComponent<SpriteRenderer>().sprite.bounds.min.y;
				float maxX1 = VirtualApp.GetComponent<SpriteRenderer>().sprite.bounds.max.x;
				float maxY1 = VirtualApp.GetComponent<SpriteRenderer>().sprite.bounds.max.x;

				float minX2 = facePos.X;
				float maxX2 = facePos.X + facePos.Width;
				float minY2 = facePos.Y;
				float maxY2 = facePos.Y + facePos.Height;
				if (rectOverlap(minX1, minY1, maxX1, maxY1, minX2, minY2, maxX2, maxY2))
				{
					// make the app transparrent
					VirtualApp.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 50);
					appIsTrans++;
				}
			}
		}

	}

	public bool rectOverlap(float minX1, float minY1, float maxX1, float maxY1, float minX2, float minY2, float maxX2, float maxY2)
	{
		if ((maxX1 <= maxX2 && maxX1 >= minX2) || (minX1 <= maxX2 && minX1 >= minX2))
		{
			if (minY1 >= maxY2)
				return false;
			if (maxY1 <= minY2)
				return false;
			return true;

		}
		else if ((maxX1 <= minX2 && maxX1 >= maxX2))
		{
			if (minY1 >= maxY2)
				return false;
			if (maxY1 <= minY2)
				return false;
			return true;

		}
		return false;
	}
}
/*
 * 
 * private bool is_conversation()
	{
		// input events run before game logic and update and this apps is running in update so isTalking must be accurate
		//Debug.Log("System is working on conversation detection");

		if (!isTalking)
		{
			// no conversation detected, we're done 
			return false;
		}

		// Run the python code to chanck there is a face and returns the location and scale of the face 
		//isFace = FaceDetection.DetectFaces(out facePos, out faceScale, out confidence);
		if (isFace)
		{
			// then it is a conversation 
			return true;
		}
		// else no conversation detected, we just need to set isTalking to false and then we're done 
		return false;
	}

 */
