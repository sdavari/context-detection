﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.Speech;
using Microsoft.MixedReality.Toolkit.Input;

public class SpeechHandler : MonoBehaviour, IMixedRealitySpeechHandler
{
    [SerializeField]
    private DictationRecognizer dictationRecognizer;
    private static string deviceName = string.Empty;
    private int samplingRate;
    private const int messageLength = 15;
    // Use this string to cache the text currently displayed in the text box.
    private StringBuilder textSoFar;
    //private StringBuilder currentlySaying;
    ContextAwareManager contextAwareManager;
 
    private void Awake()
    {
        contextAwareManager = GameObject.Find("Manager").GetComponent<ContextAwareManager>();
       
        dictationRecognizer = new DictationRecognizer();

        // 3.a: Register for dictationRecognizer.DictationHypothesis and implement DictationHypothesis below
        // This event is fired while the user is talking. As the recognizer listens, it provides text of what it's heard so far.
        dictationRecognizer.DictationHypothesis += DictationRecognizer_DictationHypothesis;

        // 3.a: Register for dictationRecognizer.DictationResult and implement DictationResult below
        // This event is fired after the user pauses, typically at the end of a sentence. The full recognized string is returned here.
        dictationRecognizer.DictationResult += DictationRecognizer_DictationResult;

        // 3.a: Register for dictationRecognizer.DictationComplete and implement DictationComplete below
        // This event is fired when the recognizer stops, whether from Stop() being called, a timeout occurring, or some other error.
        dictationRecognizer.DictationComplete += DictationRecognizer_DictationComplete;
        
        // For Future maybe
        //currentlySaying = new StringBuilder();
        textSoFar = new StringBuilder("");

        Debug.Log(" contextAwareManager.isTalking: " + contextAwareManager.isTalking);

        PhraseRecognitionSystem.Shutdown();
        dictationRecognizer.Start();
        // Query the maximum frequency of the default microphone. Use 'unused' to ignore the minimum frequency.
        int unused;
        Microphone.GetDeviceCaps(deviceName, out unused, out samplingRate);

        // Query the maximum frequency of the default microphone. Use 'unused' to ignore the minimum frequency.
        //int unused;
        //Microphone.GetDeviceCaps(deviceName, out unused, out samplingRate);
        //Microphone.Start(deviceName, false, messageLength, samplingRate);
    }

    /// <summary>
    /// This event is fired while the user is talking. As the recognizer listens, it provides text of what it's heard so far.
    /// </summary>
    /// <param name="text">The currently hypothesized recognition.</param>
    private void DictationRecognizer_DictationHypothesis(string text)
    {
        if (text != null && text.Length != 0)
            contextAwareManager.isTalking = true;
        //currentlySaying.Append(text);
        textSoFar.Append(text);
    }

    /// <summary>
    /// This event is fired after the user pauses, typically at the end of a sentence. The full recognized string is returned here.
    /// </summary>
    /// <param name="text">The text that was heard by the recognizer.</param>
    /// <param name="confidence">A representation of how confident (rejected, low, medium, high) the recognizer is of this recognition.</param>
    private void DictationRecognizer_DictationResult(string text, ConfidenceLevel confidence)
    {
        // 3.a: Append textSoFar with latest text
        if (text != null && text.Length != 0)
            contextAwareManager.isTalking = true;
        textSoFar.Append(text + ". ");
    }

    /// <summary>
    /// This event is fired when the recognizer stops, whether from Stop() being called, a timeout occurring, or some other error.
    /// Typically, this will simply return "Complete". In this case, we check to see if the recognizer timed out.
    /// </summary>
    /// <param name="cause">An enumerated reason for the session completing.</param>
    private void DictationRecognizer_DictationComplete(DictationCompletionCause cause)
    {
        // If Timeout occurs, the user has been silent for too long.
        // With dictation, the default timeout after a recognition is 20 seconds.
        // The default timeout with initial silence is 5 seconds.
        if (cause == DictationCompletionCause.TimeoutExceeded)
        {
            Microphone.End(deviceName);
            contextAwareManager.isTalking = false;
            //currentlySaying = new StringBuilder();
            textSoFar = new StringBuilder("");
        }
    }

    public void StopRecording()
    {
        // 3.a: Check if dictationRecognizer.Status is Running and stop it if so
        if (dictationRecognizer.Status == SpeechSystemStatus.Running)
        {
            dictationRecognizer.Stop();
        }
        Microphone.End(deviceName);
    }
    public AudioClip StartRecording()
    {
        // 3.a Shutdown the PhraseRecognitionSystem. This controls the KeywordRecognizers
        PhraseRecognitionSystem.Shutdown();

        // 3.a: Start dictationRecognizer
        dictationRecognizer.Start();

        // Start recording from the microphone for 10 seconds.
        return Microphone.Start(deviceName, false, messageLength, samplingRate);
    }
    private void Update()
    {
        if (textSoFar != null && textSoFar.Length != 0)
            contextAwareManager.isTalking = true;
        if (!contextAwareManager.isTalking)
        {
            dictationRecognizer.Start();
        }
    }

    /// <summary>
    /// The dictation recognizer may not turn off immediately, so this call blocks on
    /// the recognizer reporting that it has actually stopped.
    /// </summary>
    public IEnumerator WaitForDictationToStop()
    {
        while (dictationRecognizer != null && dictationRecognizer.Status == SpeechSystemStatus.Running)
        {
            yield return null;
        }
    }

    void IMixedRealitySpeechHandler.OnSpeechKeywordRecognized(SpeechEventData eventData)
    {
        //contextAwareManager.isTalking = true;
        Debug.Log("OnSpeechKeywordRecognized: " + contextAwareManager.isTalking);

    }
}
