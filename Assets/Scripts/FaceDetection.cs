﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

using System.Collections.Generic;
using System.Diagnostics;
using OpenCvSharp.Demo;
using Debug = UnityEngine.Debug;
using HoloLensCameraStream;
using UnityEngine.UI;
using System.Linq;
using UnityEngine;
using OpenCvSharp;
using System;

public class FaceDetection : MonoBehaviour
{
	public TextAsset faces;
	public TextAsset eyes;
	event OnVideoCaptureResourceCreatedCallback VideoCaptureCreated;
	static VideoCapture videoCapture;
	static FaceDetection instance;
	public RawImage rawImage;
	byte[] _latestImageBytes;
	HoloLensCameraStream.Resolution _resolution;

	protected CascadeClassifier cascadeFaces = null;
	protected CascadeClassifier cascadeEyes = null;
	protected Mat processingImage = null;
	public Mat Image { get; private set; }
	//private List<OpenCvSharp.Demo.DetectedFace> Faces { get; private set; }

	public void SetResolution(int width, int height)
	{
		var texture = new Texture2D(width, height, TextureFormat.BGRA32, false);
		rawImage.texture = texture;
	}
	public void SetBytes(byte[] image)
	{
		var texture = rawImage.texture as Texture2D;
		texture.LoadRawTextureData(image); //TODO: Should be able to do this: texture.LoadRawTextureData(pointerToImage, 1280 * 720 * 4);
		texture.Apply();
	}


	public static FaceDetection Instance
	{
		get
		{
			return instance;
		}
	}

	public void SetNativeISpatialCoordinateSystemPtr(IntPtr ptr)
	{
		videoCapture.WorldOriginPtr = ptr;
	}

	public void GetVideoCaptureAsync(OnVideoCaptureResourceCreatedCallback onVideoCaptureAvailable)
	{
		if (onVideoCaptureAvailable == null)
		{
			Debug.LogError("You must supply the onVideoCaptureAvailable delegate.");
		}

		if (videoCapture == null)
		{
			VideoCaptureCreated += onVideoCaptureAvailable;
		}
		else
		{
			onVideoCaptureAvailable(videoCapture);
		}
	}

	public HoloLensCameraStream.Resolution GetHighestResolution()
	{
		if (videoCapture == null)
		{
			throw new Exception("Please call this method after a VideoCapture instance has been created.");
		}
		return videoCapture.GetSupportedResolutions().OrderByDescending((r) => r.width * r.height).FirstOrDefault();
	}

	public HoloLensCameraStream.Resolution GetLowestResolution()
	{
		if (videoCapture == null)
		{
			throw new Exception("Please call this method after a VideoCapture instance has been created.");
		}
		return videoCapture.GetSupportedResolutions().OrderBy((r) => r.width * r.height).FirstOrDefault();
	}

	public float GetHighestFrameRate(HoloLensCameraStream.Resolution forResolution)
	{
		if (videoCapture == null)
		{
			throw new Exception("Please call this method after a VideoCapture instance has been created.");
		}
		return videoCapture.GetSupportedFrameRatesForResolution(forResolution).OrderByDescending(r => r).FirstOrDefault();
	}

	public float GetLowestFrameRate(HoloLensCameraStream.Resolution forResolution)
	{
		if (videoCapture == null)
		{
			throw new Exception("Please call this method after a VideoCapture instance has been created.");
		}
		return videoCapture.GetSupportedFrameRatesForResolution(forResolution).OrderBy(r => r).FirstOrDefault();
	}

	private void Awake()
	{
		if (instance != null)
		{
			Debug.LogError("Cannot create two instances of CamStreamManager.");
			return;
		}

		instance = this;
		VideoCapture.CreateAync(OnVideoCaptureInstanceCreated);
	}

	private void OnDestroy()
	{
		if (videoCapture != null)
		{
			videoCapture.Dispose();
		}
		if (instance == this)
		{
			instance = null;
		}
	}

	private void OnVideoCaptureInstanceCreated(VideoCapture videoCapture)
	{
		if (videoCapture == null)
		{
			Debug.LogError("Creating the VideoCapture object failed.");
			return;
		}

		FaceDetection.videoCapture = videoCapture;
		if (VideoCaptureCreated != null)
		{
			VideoCaptureCreated(videoCapture);
		}
		_resolution = FaceDetection.Instance.GetLowestResolution();
		float frameRate = FaceDetection.Instance.GetHighestFrameRate(_resolution);
		UnityEngine.WSA.Application.InvokeOnAppThread(() => { SetResolution(_resolution.width, _resolution.height); }, false);
	}

	void OnFrameSampleAcquired(VideoCaptureSample sample)
	{
		//When copying the bytes out of the buffer, you must supply a byte[] that is appropriately sized.
		//You can reuse this byte[] until you need to resize it (for whatever reason).
		if (_latestImageBytes == null || _latestImageBytes.Length < sample.dataLength)
		{
			_latestImageBytes = new byte[sample.dataLength];
		}
		sample.CopyRawImageDataIntoBuffer(_latestImageBytes);

		//If you need to get the cameraToWorld matrix for purposes of compositing you can do it like this
		float[] cameraToWorldMatrix;
		if (sample.TryGetCameraToWorldMatrix(out cameraToWorldMatrix) == false)
		{
			return;
		}

		//If you need to get the projection matrix for purposes of compositing you can do it like this
		float[] projectionMatrix;
		if (sample.TryGetProjectionMatrix(out projectionMatrix) == false)
		{
			return;
		}

		sample.Dispose();

		//This is where we actually use the image data
		UnityEngine.WSA.Application.InvokeOnAppThread(() =>
		{
			SetBytes(_latestImageBytes);
		}, false);
	}
	public static bool DetectFaces(out OpenCvSharp.Rect facePos, out float faceScale, out float confidence)
	{
		facePos = new OpenCvSharp.Rect();
		faceScale = 0.0f;
		confidence = 0.85f;
		






		return false;
	}
	// convert and prepare
	/*
	 * Image = MatFromTexture(texture, texParams);
	public virtual void ProcessTexture()
	{
		// convert Unity texture to OpenCv::Mat
		ImportTexture(texture, texParams);

		// detect
		if (detect)
		{
			double invF = 1.0 / appliedFactor;
			DataStabilizer.ThresholdFactor = invF;

			// convert to grayscale and normalize
			Mat gray = new Mat();
			Cv2.CvtColor(processingImage, gray, ColorConversionCodes.BGR2GRAY);

			// fix shadows
			Cv2.EqualizeHist(gray, gray);

			Mat normalized = new Mat();
			CLAHE clahe = CLAHE.Create();
			clahe.TilesGridSize = new Size(8, 8);
			clahe.Apply(gray, normalized);
			gray = normalized;
			 

			// detect matching regions (faces bounding)
			Rect[] rawFaces = cascadeFaces.DetectMultiScale(gray, 1.2, 6);
			if (Faces.Count != rawFaces.Length)
				Faces.Clear();

			// now per each detected face draw a marker and detect eyes inside the face rect
			int facesCount = 0;
			for (int i = 0; i < rawFaces.Length; ++i)
			{
				Rect faceRect = rawFaces[i];
				Rect faceRectScaled = faceRect * invF;
				using (Mat grayFace = new Mat(gray, faceRect))
				{
					// another trick: confirm the face with eye detector, will cut some false positives
					if (cutFalsePositivesWithEyesSearch && null != cascadeEyes)
					{
						Rect[] eyes = cascadeEyes.DetectMultiScale(grayFace);
						if (eyes.Length == 0 || eyes.Length > 2)
							continue;
					}

					// get face object
					DetectedFace face = null;
					if (Faces.Count < i + 1)
					{
						face = new DetectedFace(DataStabilizer, faceRectScaled);
						Faces.Add(face);
					}
					else
					{
						face = Faces[i];
						face.SetRegion(faceRectScaled);
					}

					// shape
					facesCount++;
					if (null != shapeFaces)
					{
						Point[] marks = shapeFaces.DetectLandmarks(gray, faceRect);

						// we have 68-point predictor
						if (marks.Length == 68)
						{
							// transform landmarks to the original image space
							List<Point> converted = new List<Point>();
							foreach (Point pt in marks)
								converted.Add(pt * invF);

							// save and parse landmarks
							face.SetLandmarks(converted.ToArray());
						}
					}
				}
			}

			// log
			//UnityEngine.Debug.Log(String.Format("Found {0} faces", Faces.Count));
		}
	}

	*/
}


/*
 * From Yolo
 * var org = Cv2.ImRead(file);
	var w = org.Width;
	var h = org.Height;
	//setting blob, parameter are important
	var blob = CvDnn.BlobFromImage(org, 1 / 255.0, new Size(544, 544), new Scalar(), true, false);
	var net = CvDnn.ReadNetFromDarknet(cfg, model);
	net.SetInput(blob, "data");

	Stopwatch sw = new Stopwatch();
	sw.Start();
	//forward model
	var prob = net.Forward();
	sw.Stop();
	Console.WriteLine($"Runtime:{sw.ElapsedMilliseconds} ms");

		
	const int prefix = 5;   //skip 0~4

	for (int i = 0; i < prob.Rows; i++)
	{
		confidence = prob.At<float>(i, 4);
		if (confidence > threshold)
		{
			//get classes probability
			Cv2.MinMaxLoc(prob.Row[i].ColRange(prefix, prob.Cols), out _, out Point max);
			var classes = max.X;
			var probability = prob.At<float>(i, classes + prefix);

			if (probability > threshold) //more accuracy
			{
				//get center and width/height
				var centerX = prob.At<float>(i, 0) * w;
				var centerY = prob.At<float>(i, 1) * h;
				var width = prob.At<float>(i, 2) * w;
				var height = prob.At<float>(i, 3) * h;
				//label formating
				var label = $"{Labels[classes]} {probability * 100:0.00}%";
				Console.WriteLine($"confidence {confidence * 100:0.00}% {label}");
				var x1 = (centerX - width / 2) < 0 ? 0 : centerX - width / 2; //avoid left side over edge
																				//draw result
				org.Rectangle(new Point(x1, centerY - height / 2), new Point(centerX + width / 2, centerY + height / 2), Colors[classes], 2);
				var textSize = Cv2.GetTextSize(label, HersheyFonts.HersheyTriplex, 0.5, 1, out var baseline);
				Cv2.Rectangle(org, new OpenCvSharp.Rect(new Point(x1, centerY - height / 2 - textSize.Height - baseline),
						new Size(textSize.Width, textSize.Height + baseline)), Colors[classes], Cv2.FILLED);
				Cv2.PutText(org, label, new Point(x1, centerY - height / 2 - baseline), HersheyFonts.HersheyTriplex, 0.5, Scalar.Black);
			}
		}
		using (new Window("died.tw", org))
		{
			Cv2.WaitKey();
		}
	}

 */


/* 
 * This works 2D on  Webcam
 * 
public class FaceDetection : WebCamera
{
	public TextAsset faces;
	public TextAsset eyes;
	//WebCamTexture _webCamTexture;
	static FaceProcessor<WebCamTexture> faceProcessor;
	public FaceDetection(TextAsset faces_in, TextAsset eyes_in)
	{
		faces = faces_in;
		eyes = eyes_in;
	}

	protected override void Awake()
	{
		Debug.Log("Awake");
		base.Awake();
		faceProcessor = new FaceProcessor<WebCamTexture>();
		faceProcessor.Initialize(faces.text, eyes.text);
		// data stabilizer - affects face rects, face landmarks etc.
		faceProcessor.DataStabilizer.Enabled = true;        // enable stabilizer
		faceProcessor.DataStabilizer.Threshold = 2.0;       // threshold value in pixels
		faceProcessor.DataStabilizer.SamplesCount = 2;      // how many samples do we need to compute stable data

		// performance data - some tricks to make it work faster
		faceProcessor.Performance.Downscale = 256;          // processed image is pre-scaled down to N px by long side
		faceProcessor.Performance.SkipRate = 0;             // we actually process only each Nth frame (and every frame for skipRate = 0)

		//var display = Windows.Graphics.Holographic.HolographicDisplay.GetDefault();
		//var view = display.TryGetViewConfiguration(Windows.Graphics.Holographic.HolographicViewConfiguration.PhotoVideoCamera);
		//if (view != null)
		//{
		//	view.IsEnabled = true;
		//}
	}

	public static bool DetectFaces(out OpenCvSharp.Rect facePos, out float faceScale, out float confidence)
	{
		facePos = new OpenCvSharp.Rect();
		faceScale = 0.0f;
		confidence = 0.85f;

		Debug.Log("number of faces: " + faceProcessor.Faces.Count);
		//faceProcessor.Faces
		if (faceProcessor.Faces.Count != 0)
		{
			
			foreach (DetectedFace face in faceProcessor.Faces)
			{
				// return the first faces pos
				Debug.Log("Face region: " + face.Region);
				facePos = face.Region;
				return true;
				// if face.Region is blocking content
				//Cv2.Rectangle((InputOutputArray)faceProcessor.Image, face.Region, Scalar.FromRgb(255, 0, 0), 2);
			}
		}
		return false;
		
	}

	protected override bool ProcessTexture(WebCamTexture input, ref Texture2D output)
	{
		Debug.Log("ProcessTexture FaceDetection");
		// detect everything we're interested in
		faceProcessor.ProcessTexture(input, TextureParameters);

		// mark detected objects
		faceProcessor.MarkDetected();

		// processor.Image now holds data we'd like to visualize
		output = OpenCvSharp.Unity.MatToTexture(faceProcessor.Image, output);   // if output is valid texture it's buffer will be re-used, otherwise it will be re-created

		return true;
	}
}


	*/



/*
 * using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IronPython.Hosting;

public class FaceDetection : MonoBehaviour
{
	public static bool detectFaces(out Vector3 facePos, out float faceScale, out float confidence)
	{
		bool isFace = false;
		facePos = new Vector3(0, 0, 0);
		faceScale = 0;
		// see if you wanna use confidence in future
		confidence = 70.0f;

		var engine = Python.CreateEngine();
		ICollection<string> searchPaths = engine.GetSearchPaths();
		searchPaths.Add(@"D:\Personal_Folders\Shakiba\20Spring\deep-learning-face-detection\deep-learning-face-detection\");
		//Path to the Python standard library
		searchPaths.Add(@"D:\Personal_Folders\Shakiba\20Spring\2.ContextAware\context-detection\Assets\Plugins\Lib");
		engine.SetSearchPaths(searchPaths);
		dynamic py = engine.ExecuteFile(@"D:\Personal_Folders\Shakiba\20Spring\deep-learning-face-detection\deep-learning-face-detection\detect_faces_video.py");
		dynamic DetectFaces = py.DetectFaces();
		DetectFaces.detect_faces();


		Debug.Log("System is working on Face detection");
		// Run the python code to chack if there is face and returns the location of the face and its position;		
		

		return isFace;	
	}
}


*/
