﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000008 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000009 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Intersect(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::IntersectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000010 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000012 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000015 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000016 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000017 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001A System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001B System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000001C TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x0000001D System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583 ();
// 0x0000001E System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000001F TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000020 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000021 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000022 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000023 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000024 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000026 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000027 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000028 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000029 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000002A System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000002B System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000002C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000002E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000002F System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000030 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000031 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000033 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000034 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000036 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000037 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x00000038 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x00000039 System.Boolean System.Linq.Enumerable_<TakeIterator>d__25`1::MoveNext()
// 0x0000003A System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::<>m__Finally1()
// 0x0000003B TSource System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000003C System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x0000003D System.Object System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x0000003E System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000003F System.Collections.IEnumerator System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000040 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x00000041 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x00000042 System.Boolean System.Linq.Enumerable_<UnionIterator>d__71`1::MoveNext()
// 0x00000043 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally1()
// 0x00000044 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally2()
// 0x00000045 TSource System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000046 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x00000047 System.Object System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x00000048 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000049 System.Collections.IEnumerator System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000004A System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::.ctor(System.Int32)
// 0x0000004B System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::System.IDisposable.Dispose()
// 0x0000004C System.Boolean System.Linq.Enumerable_<IntersectIterator>d__74`1::MoveNext()
// 0x0000004D System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::<>m__Finally1()
// 0x0000004E TSource System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000004F System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.IEnumerator.Reset()
// 0x00000050 System.Object System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.IEnumerator.get_Current()
// 0x00000051 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000052 System.Collections.IEnumerator System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000053 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000054 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x00000055 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000056 System.Boolean System.Linq.Set`1::Remove(TElement)
// 0x00000057 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000058 System.Void System.Linq.Set`1::Resize()
// 0x00000059 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x0000005A System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000005B System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000005C System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005D System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000005E System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000005F System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000060 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000061 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000062 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000063 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000064 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000065 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000066 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000067 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000068 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000069 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000006A System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000006B System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x0000006C System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000006D System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000006E System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000006F System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000070 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000071 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000072 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000073 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000074 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000075 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000076 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000077 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000078 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000079 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000007A System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000007B System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000007C System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000007D System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007E System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000007F System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000080 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000081 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000082 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000083 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000084 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000085 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000086 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000087 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000088 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000089 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000008A System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000008B T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x0000008C System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000008D System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[141] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[141] = 
{
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	102,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[42] = 
{
	{ 0x02000004, { 68, 4 } },
	{ 0x02000005, { 72, 9 } },
	{ 0x02000006, { 81, 7 } },
	{ 0x02000007, { 88, 10 } },
	{ 0x02000008, { 98, 1 } },
	{ 0x02000009, { 99, 8 } },
	{ 0x0200000A, { 107, 12 } },
	{ 0x0200000B, { 119, 12 } },
	{ 0x0200000D, { 131, 8 } },
	{ 0x0200000F, { 139, 3 } },
	{ 0x02000010, { 144, 5 } },
	{ 0x02000011, { 149, 7 } },
	{ 0x02000012, { 156, 3 } },
	{ 0x02000013, { 159, 7 } },
	{ 0x02000014, { 166, 4 } },
	{ 0x02000015, { 170, 21 } },
	{ 0x02000017, { 191, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 5 } },
	{ 0x06000007, { 15, 1 } },
	{ 0x06000008, { 16, 2 } },
	{ 0x06000009, { 18, 2 } },
	{ 0x0600000A, { 20, 2 } },
	{ 0x0600000B, { 22, 1 } },
	{ 0x0600000C, { 23, 1 } },
	{ 0x0600000D, { 24, 2 } },
	{ 0x0600000E, { 26, 1 } },
	{ 0x0600000F, { 27, 2 } },
	{ 0x06000010, { 29, 3 } },
	{ 0x06000011, { 32, 2 } },
	{ 0x06000012, { 34, 4 } },
	{ 0x06000013, { 38, 4 } },
	{ 0x06000014, { 42, 4 } },
	{ 0x06000015, { 46, 3 } },
	{ 0x06000016, { 49, 3 } },
	{ 0x06000017, { 52, 1 } },
	{ 0x06000018, { 53, 3 } },
	{ 0x06000019, { 56, 2 } },
	{ 0x0600001A, { 58, 2 } },
	{ 0x0600001B, { 60, 5 } },
	{ 0x0600001C, { 65, 3 } },
	{ 0x0600005D, { 142, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[193] = 
{
	{ (Il2CppRGCTXDataType)2, 44189 },
	{ (Il2CppRGCTXDataType)3, 43707 },
	{ (Il2CppRGCTXDataType)2, 44190 },
	{ (Il2CppRGCTXDataType)2, 44191 },
	{ (Il2CppRGCTXDataType)3, 43708 },
	{ (Il2CppRGCTXDataType)2, 44192 },
	{ (Il2CppRGCTXDataType)2, 44193 },
	{ (Il2CppRGCTXDataType)3, 43709 },
	{ (Il2CppRGCTXDataType)2, 44194 },
	{ (Il2CppRGCTXDataType)3, 43710 },
	{ (Il2CppRGCTXDataType)2, 44195 },
	{ (Il2CppRGCTXDataType)3, 43711 },
	{ (Il2CppRGCTXDataType)3, 43712 },
	{ (Il2CppRGCTXDataType)2, 35239 },
	{ (Il2CppRGCTXDataType)3, 43713 },
	{ (Il2CppRGCTXDataType)3, 43714 },
	{ (Il2CppRGCTXDataType)2, 44196 },
	{ (Il2CppRGCTXDataType)3, 43715 },
	{ (Il2CppRGCTXDataType)2, 44197 },
	{ (Il2CppRGCTXDataType)3, 43716 },
	{ (Il2CppRGCTXDataType)2, 44198 },
	{ (Il2CppRGCTXDataType)3, 43717 },
	{ (Il2CppRGCTXDataType)3, 43718 },
	{ (Il2CppRGCTXDataType)3, 43719 },
	{ (Il2CppRGCTXDataType)2, 44199 },
	{ (Il2CppRGCTXDataType)3, 43720 },
	{ (Il2CppRGCTXDataType)3, 43721 },
	{ (Il2CppRGCTXDataType)2, 44200 },
	{ (Il2CppRGCTXDataType)3, 43722 },
	{ (Il2CppRGCTXDataType)2, 44201 },
	{ (Il2CppRGCTXDataType)3, 43723 },
	{ (Il2CppRGCTXDataType)3, 43724 },
	{ (Il2CppRGCTXDataType)2, 35274 },
	{ (Il2CppRGCTXDataType)3, 43725 },
	{ (Il2CppRGCTXDataType)2, 44202 },
	{ (Il2CppRGCTXDataType)2, 44203 },
	{ (Il2CppRGCTXDataType)2, 35275 },
	{ (Il2CppRGCTXDataType)2, 44204 },
	{ (Il2CppRGCTXDataType)2, 44205 },
	{ (Il2CppRGCTXDataType)2, 44206 },
	{ (Il2CppRGCTXDataType)2, 35277 },
	{ (Il2CppRGCTXDataType)2, 44207 },
	{ (Il2CppRGCTXDataType)2, 44208 },
	{ (Il2CppRGCTXDataType)2, 44209 },
	{ (Il2CppRGCTXDataType)2, 35279 },
	{ (Il2CppRGCTXDataType)2, 44210 },
	{ (Il2CppRGCTXDataType)2, 35281 },
	{ (Il2CppRGCTXDataType)2, 44211 },
	{ (Il2CppRGCTXDataType)3, 43726 },
	{ (Il2CppRGCTXDataType)2, 44212 },
	{ (Il2CppRGCTXDataType)2, 35284 },
	{ (Il2CppRGCTXDataType)2, 44213 },
	{ (Il2CppRGCTXDataType)2, 35286 },
	{ (Il2CppRGCTXDataType)2, 35288 },
	{ (Il2CppRGCTXDataType)2, 44214 },
	{ (Il2CppRGCTXDataType)3, 43727 },
	{ (Il2CppRGCTXDataType)2, 44215 },
	{ (Il2CppRGCTXDataType)2, 35291 },
	{ (Il2CppRGCTXDataType)2, 44216 },
	{ (Il2CppRGCTXDataType)3, 43728 },
	{ (Il2CppRGCTXDataType)3, 43729 },
	{ (Il2CppRGCTXDataType)2, 44217 },
	{ (Il2CppRGCTXDataType)2, 35295 },
	{ (Il2CppRGCTXDataType)2, 44218 },
	{ (Il2CppRGCTXDataType)2, 35297 },
	{ (Il2CppRGCTXDataType)2, 35298 },
	{ (Il2CppRGCTXDataType)2, 44219 },
	{ (Il2CppRGCTXDataType)3, 43730 },
	{ (Il2CppRGCTXDataType)3, 43731 },
	{ (Il2CppRGCTXDataType)3, 43732 },
	{ (Il2CppRGCTXDataType)2, 35304 },
	{ (Il2CppRGCTXDataType)3, 43733 },
	{ (Il2CppRGCTXDataType)3, 43734 },
	{ (Il2CppRGCTXDataType)2, 35313 },
	{ (Il2CppRGCTXDataType)2, 44220 },
	{ (Il2CppRGCTXDataType)3, 43735 },
	{ (Il2CppRGCTXDataType)3, 43736 },
	{ (Il2CppRGCTXDataType)2, 35315 },
	{ (Il2CppRGCTXDataType)2, 44011 },
	{ (Il2CppRGCTXDataType)3, 43737 },
	{ (Il2CppRGCTXDataType)3, 43738 },
	{ (Il2CppRGCTXDataType)3, 43739 },
	{ (Il2CppRGCTXDataType)2, 35322 },
	{ (Il2CppRGCTXDataType)2, 44221 },
	{ (Il2CppRGCTXDataType)3, 43740 },
	{ (Il2CppRGCTXDataType)3, 43741 },
	{ (Il2CppRGCTXDataType)3, 42332 },
	{ (Il2CppRGCTXDataType)3, 43742 },
	{ (Il2CppRGCTXDataType)3, 43743 },
	{ (Il2CppRGCTXDataType)2, 35331 },
	{ (Il2CppRGCTXDataType)2, 44222 },
	{ (Il2CppRGCTXDataType)3, 43744 },
	{ (Il2CppRGCTXDataType)3, 43745 },
	{ (Il2CppRGCTXDataType)3, 43746 },
	{ (Il2CppRGCTXDataType)3, 43747 },
	{ (Il2CppRGCTXDataType)3, 43748 },
	{ (Il2CppRGCTXDataType)3, 42338 },
	{ (Il2CppRGCTXDataType)3, 43749 },
	{ (Il2CppRGCTXDataType)3, 43750 },
	{ (Il2CppRGCTXDataType)3, 43751 },
	{ (Il2CppRGCTXDataType)2, 35351 },
	{ (Il2CppRGCTXDataType)2, 35346 },
	{ (Il2CppRGCTXDataType)3, 43752 },
	{ (Il2CppRGCTXDataType)2, 35345 },
	{ (Il2CppRGCTXDataType)2, 44223 },
	{ (Il2CppRGCTXDataType)3, 43753 },
	{ (Il2CppRGCTXDataType)3, 43754 },
	{ (Il2CppRGCTXDataType)3, 43755 },
	{ (Il2CppRGCTXDataType)3, 43756 },
	{ (Il2CppRGCTXDataType)2, 44224 },
	{ (Il2CppRGCTXDataType)3, 43757 },
	{ (Il2CppRGCTXDataType)2, 35364 },
	{ (Il2CppRGCTXDataType)2, 35356 },
	{ (Il2CppRGCTXDataType)3, 43758 },
	{ (Il2CppRGCTXDataType)3, 43759 },
	{ (Il2CppRGCTXDataType)2, 35355 },
	{ (Il2CppRGCTXDataType)2, 44225 },
	{ (Il2CppRGCTXDataType)3, 43760 },
	{ (Il2CppRGCTXDataType)3, 43761 },
	{ (Il2CppRGCTXDataType)3, 43762 },
	{ (Il2CppRGCTXDataType)2, 44226 },
	{ (Il2CppRGCTXDataType)3, 43763 },
	{ (Il2CppRGCTXDataType)2, 35377 },
	{ (Il2CppRGCTXDataType)2, 35369 },
	{ (Il2CppRGCTXDataType)3, 43764 },
	{ (Il2CppRGCTXDataType)3, 43765 },
	{ (Il2CppRGCTXDataType)3, 43766 },
	{ (Il2CppRGCTXDataType)2, 35368 },
	{ (Il2CppRGCTXDataType)2, 44227 },
	{ (Il2CppRGCTXDataType)3, 43767 },
	{ (Il2CppRGCTXDataType)3, 43768 },
	{ (Il2CppRGCTXDataType)3, 43769 },
	{ (Il2CppRGCTXDataType)2, 44228 },
	{ (Il2CppRGCTXDataType)2, 44229 },
	{ (Il2CppRGCTXDataType)3, 43770 },
	{ (Il2CppRGCTXDataType)3, 43771 },
	{ (Il2CppRGCTXDataType)2, 35389 },
	{ (Il2CppRGCTXDataType)3, 43772 },
	{ (Il2CppRGCTXDataType)2, 35390 },
	{ (Il2CppRGCTXDataType)2, 44230 },
	{ (Il2CppRGCTXDataType)3, 43773 },
	{ (Il2CppRGCTXDataType)3, 43774 },
	{ (Il2CppRGCTXDataType)2, 44231 },
	{ (Il2CppRGCTXDataType)3, 43775 },
	{ (Il2CppRGCTXDataType)2, 44232 },
	{ (Il2CppRGCTXDataType)3, 43776 },
	{ (Il2CppRGCTXDataType)3, 43777 },
	{ (Il2CppRGCTXDataType)3, 43778 },
	{ (Il2CppRGCTXDataType)2, 35409 },
	{ (Il2CppRGCTXDataType)3, 43779 },
	{ (Il2CppRGCTXDataType)2, 35417 },
	{ (Il2CppRGCTXDataType)3, 43780 },
	{ (Il2CppRGCTXDataType)2, 44233 },
	{ (Il2CppRGCTXDataType)2, 44234 },
	{ (Il2CppRGCTXDataType)3, 43781 },
	{ (Il2CppRGCTXDataType)3, 43782 },
	{ (Il2CppRGCTXDataType)3, 43783 },
	{ (Il2CppRGCTXDataType)3, 43784 },
	{ (Il2CppRGCTXDataType)3, 43785 },
	{ (Il2CppRGCTXDataType)3, 43786 },
	{ (Il2CppRGCTXDataType)2, 35433 },
	{ (Il2CppRGCTXDataType)2, 44235 },
	{ (Il2CppRGCTXDataType)3, 43787 },
	{ (Il2CppRGCTXDataType)3, 43788 },
	{ (Il2CppRGCTXDataType)2, 35437 },
	{ (Il2CppRGCTXDataType)3, 43789 },
	{ (Il2CppRGCTXDataType)2, 44236 },
	{ (Il2CppRGCTXDataType)2, 35447 },
	{ (Il2CppRGCTXDataType)2, 35445 },
	{ (Il2CppRGCTXDataType)2, 44237 },
	{ (Il2CppRGCTXDataType)3, 43790 },
	{ (Il2CppRGCTXDataType)2, 44238 },
	{ (Il2CppRGCTXDataType)3, 43791 },
	{ (Il2CppRGCTXDataType)3, 43792 },
	{ (Il2CppRGCTXDataType)3, 43793 },
	{ (Il2CppRGCTXDataType)2, 35451 },
	{ (Il2CppRGCTXDataType)3, 43794 },
	{ (Il2CppRGCTXDataType)3, 43795 },
	{ (Il2CppRGCTXDataType)2, 35454 },
	{ (Il2CppRGCTXDataType)3, 43796 },
	{ (Il2CppRGCTXDataType)1, 44239 },
	{ (Il2CppRGCTXDataType)2, 35453 },
	{ (Il2CppRGCTXDataType)3, 43797 },
	{ (Il2CppRGCTXDataType)1, 35453 },
	{ (Il2CppRGCTXDataType)1, 35451 },
	{ (Il2CppRGCTXDataType)2, 44240 },
	{ (Il2CppRGCTXDataType)2, 35453 },
	{ (Il2CppRGCTXDataType)3, 43798 },
	{ (Il2CppRGCTXDataType)3, 43799 },
	{ (Il2CppRGCTXDataType)3, 43800 },
	{ (Il2CppRGCTXDataType)2, 35452 },
	{ (Il2CppRGCTXDataType)3, 43801 },
	{ (Il2CppRGCTXDataType)2, 35465 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	141,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	42,
	s_rgctxIndices,
	193,
	s_rgctxValues,
	NULL,
};
